# Copyright 2015 Oliver Serang
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

import numpy as np
from scipy.signal import convolve, fftconvolve
import itertools

def argmax(lst):
    # Note: returns smallest index in case of a tie
    return max( [ (val,ind) for ind,val in enumerate(lst) ] )[1]

def argmin(lst):
    # Note: returns smallest index in case of a tie
    return min( [ (val,ind) for ind,val in enumerate(lst) ] )[1]

def normalized(arrayVec):
    return np.array(np.fabs(arrayVec), np.float) / sum(np.fabs(arrayVec))

def maxNormalized(arrayVec):
    return np.array(np.fabs(arrayVec), np.float) / max(np.fabs(arrayVec))

def median(lst):
    # When len(lst) is even, gives first of the two equally middle
    # indices:
    middleIndex = (len(lst)-1)/2
    # Note: could be performed faster in O(n) using median of medians;
    # this is simpler:
    return sorted(lst)[ middleIndex ]

def argMedian(lst):
    # When len(lst) is even, gives first of the two equally middle
    # indices:
    middleIndex = (len(lst)-1)/2
    # Note: could be performed faster in O(n) using median of medians;
    # this is simpler:
    lstAndIndices = [ (val,ind) for ind,val in enumerate(lst) ]
    middleVal,middleInd = sorted(lstAndIndices)[ middleIndex ]
    return middleInd

def prod(lst):
    result = 1.0
    for val in lst:
        result *= val
    return result

def arrLen(arr):
    return prod(arr.shape)
    
def naiveConvolutionCost(arrayA, arrayB):
    nA = arrLen(arrayA)
    nB = arrLen(arrayB)
    return nA * nB

def fftConvolutionCost(arrayA, arrayB):
    nA = arrLen(arrayA)
    nB = arrLen(arrayB)
    n = max(nA, nB)
    return n*np.log2(n)

def fftMaxConvolutionCost(arrayA, arrayB):
    nA = arrLen(arrayA)
    nB = arrLen(arrayB)
    n = max(nA, nB)
    return n*np.log2(n)*np.log2(np.log2(n))

# Generalized to work on tensors:
def maxConvolveAtIndex(arrA, arrB, indexTuple):
    if not isinstance(indexTuple, tuple):
        indexTuple = (indexTuple,)
    shapeA = arrA.shape
    shapeB = arrB.shape
    # Limits are inclusive:
    upperLimitsFromA = [ lim1-1 for lim1 in shapeA ]
    upperLimitsFromB = indexTuple
    lowerLimitsFromB = [ indexTuple[d]+1-shapeB[d] for d in xrange(len(indexTuple)) ]
    lowerLimits = [ max(0,lim1) for lim1 in lowerLimitsFromB ]
    upperLimits = [ min(lim1,lim2) for lim1,lim2 in zip(upperLimitsFromA, upperLimitsFromB) ]
    # Add +1 to upper because limits are inclusive:
    aSlice = tuple( [ np.s_[lower:upper+1] for lower,upper in zip(lowerLimits,upperLimits) ] )
    bSlice = tuple( [ np.s_[ind-upper:ind-lower+1] for ind,lower,upper in zip(indexTuple,lowerLimits,upperLimits) ] )
    return max(arrA[aSlice].flatten()*arrB[bSlice].flatten()[::-1])

def maxConvolve(vecA, vecB):
    # Note: the following line is a shortcut, and won't significantly
    # influence the runtime (compared to naive max-convolution), but
    # it's still less efficient than necessary; the new shape should
    # be vecA.shape + vecB.shape - 1 (added element-wise):
    resultShape = fftconvolve(vecA, vecB).shape
    vectorResult = np.zeros(resultShape)
    vectorResultFlat = vectorResult.flatten()
    for n,tup in zip(xrange(len(vectorResultFlat)), arrayRange(resultShape)):
        resForN = maxConvolveAtIndex(vecA, vecB, tup)
        vectorResultFlat[n] = resForN
    return np.reshape(vectorResultFlat, resultShape)
    
def fftNonnegMaxConvolveGivenPStar(xPrime, yPrime, pStar):
    ### Described in Serang 2015, Journal of Computational Biology:

    assert( min(xPrime.flatten()) >= 0.0 and min(yPrime.flatten()) >= 0.0 )
    # Check that parameters have been normalized:
    assert(np.fabs(max(xPrime.flatten())-1.0)<machinePrecisionTauFFT and np.fabs(max(yPrime.flatten())-1.0)<machinePrecisionTauFFT)
    
    aToP = np.power(xPrime, pStar)
    bToP = np.power(yPrime, pStar)

    convResult = np.fabs(fftconvolve(aToP, bToP))
    # Note, you could normalize so that the result has max value 1
    # (before multiplying in max(xPrime)*max(yPrime) and
    # max(xPrime)*max(yPrime) after multiplying in); however, when small
    # pStar is used, then the larger elements will be >1 (before
    # multiplying in maxX*maxY), and the smaller elements (which may
    # be accurately computed) will be shrunk to a smaller value below
    # the exact amount. Thus, do not force any normalization until
    # later, when each index has been computed with the most
    # appropriate pStar.
    return np.power(convResult, 1.0/pStar)

def arrayRange(shape):
  return itertools.product( *[ xrange(upperLim) for upperLim in shape ] )

def arrayEnumerate(arr):
  return zip(arrayRange(arr.shape), arr.flatten())

def flatIndexToShapedIndexTuple(flatIndex, resultShape):
    # Compute product of all but first result axis length:
    scale = 1
    for L in resultShape[1:]:
        scale *= L
    result = []
    for L in resultShape[1:]:
        indexAtI = flatIndex / scale
        flatIndex -= indexAtI * scale
        scale /= L
        result.append(indexAtI)
    result.append(flatIndex)
    return tuple(result)

machinePrecisionTauFFT=1e-12
machinePrecisionTauDivide=1e-10

# Considers a collection of pStar values and uses the highest
# numerically stable pStar at each index. If piecewiseScalingMode ==
# 'localLinear', use a local affine fit to map the approximations onto
# the exact locations. normEstimationMode controls whether the largest
# stable p-norm is used to approximate the maximum ('bigP') or whether
# a two-term norm is fit and used to extrapolate the maximum norm
# ('extrapolate'). The runtime is O( n log(n) log(log(n)) )

def fftNonnegMaxConvolvePiecewise(xPrime, yPrime, maxPStar, piecewiseScalingMode='localLinear', normEstimationMode='bigP'):
  logBase=2.0

  assert(piecewiseScalingMode in ('unscaled', 'lowerBound', 'localLinear'))
  assert(normEstimationMode in ('bigP', 'extrapolate'))

  xPrime = np.array(xPrime, np.float)
  yPrime = np.array(yPrime, np.float)
    
  assert( min(xPrime.flatten()) >= 0.0 and min(yPrime.flatten()) >= 0.0 )
  # Check that parameters have been normalized:
  assert(np.fabs(max(xPrime.flatten())-1.0)<machinePrecisionTauFFT and np.fabs(max(yPrime.flatten())-1.0)<machinePrecisionTauFFT)

  if normEstimationMode == 'bigP':
      # Note: the log base here is arbitrary (a log base lower than 2
      # would be slower and give more accurate results):
      allPStar = np.power(logBase, range(0, 1+int(round(np.log2(maxPStar)/np.log2(logBase))) ) )
  else:
      # normEstimationMode == 'extrapolate':

      # Use +2 as the upper limit instead of +1, because it means that
      # interleaving (which drops the top index), will go up to the right
      # number of indices:
      powers = np.power(logBase, range(-1, 2+int(round(np.log2(maxPStar)/np.log2(logBase))) ) )

      # Interleave, so that a 4-length evenly spaced list is always
      # available (for indices sufficiently >0):

      allPStar = np.array([ ( powers[ind], (powers[ind]+powers[ind+1])/2.0 ) for ind in xrange(len(powers)-1) ]).flatten()

  # Find the highest numerically stable pStar for each index (note:
  # this assumes that the pStar are in ascending order):
  assert(all(allPStar == sorted(allPStar)))

  # Note: Could merge with highestStablePStarIndexFlat code below, and
  # compute top down (highest pStar first). This would enable you to
  # stop as soon as highestStablePStarIndexFlat has become full (and
  # thus no smaller pStar values are needed). That would be faster
  # sometimes (when yMin / yMax is bounded below), but have the same
  # worst-case.
  resForAllPStar = np.array([ fftNonnegMaxConvolveGivenPStar(xPrime,yPrime,pStar) for pStar in allPStar ])
  resForAllPStarFlat = np.array([ v.flatten() for v in resForAllPStar ])

  resultShape = resForAllPStar[0].shape
  flattenedResultLength = len(resForAllPStar[0].flatten())

  highestStablePStarIndexFlat = np.zeros(flattenedResultLength, int)

  for pStarIndex in xrange(1,len(allPStar)):
      stableIndicesForPStar = resForAllPStarFlat[pStarIndex]**allPStar[pStarIndex] >= machinePrecisionTauFFT
      highestStablePStarIndexFlat[ stableIndicesForPStar ] = pStarIndex

  if normEstimationMode == 'extrapolate':
      # Ensure the index refers to a power of two (and not an interleaved midpoint):
      highestStablePStarIndexFlat -= highestStablePStarIndexFlat % 2

  resultFlat = resForAllPStarFlat[ highestStablePStarIndexFlat, np.arange(flattenedResultLength) ]

  allIndicesFlat = np.arange(flattenedResultLength)

  if normEstimationMode == 'extrapolate':
      # Case where only 1 stable norm estimate is available will be
      # covered automatically (resultFlat already contains highest stable estimate).

      # Case where only two points are available for norm extrapolation:
      twoPossible = allIndicesFlat[(highestStablePStarIndexFlat>1) & (highestStablePStarIndexFlat<5)]
      if len(twoPossible) > 0:
          highestStableIndicesWhenTwoPossible = highestStablePStarIndexFlat[twoPossible]
          highestStableEstimatesWhenTwoPossible = resForAllPStarFlat[highestStableIndicesWhenTwoPossible, twoPossible]
          secondHighestStableEstimatesWhenTwoPossible = resForAllPStarFlat[highestStableIndicesWhenTwoPossible-1, twoPossible]
          maxP=allPStar[highestStableIndicesWhenTwoPossible]
          deltasWhenTwoPossible = maxP / 4.0
          resultFlat[twoPossible] = linearEstimateOfMaxima(secondHighestStableEstimatesWhenTwoPossible**(maxP-deltasWhenTwoPossible), highestStableEstimatesWhenTwoPossible**maxP, deltasWhenTwoPossible)

      # Case where at least 5 points are available (thus 4 sequential points are available):
      fourPossible = allIndicesFlat[highestStablePStarIndexFlat >= 5]
      if len(fourPossible) > 0:
          highestStableIndicesWhenFourPossible = highestStablePStarIndexFlat[fourPossible]
          highestStableEstimatesWhenFourPossible = resForAllPStarFlat[highestStableIndicesWhenFourPossible, fourPossible]
          secondHighestStableEstimatesWhenFourPossible = resForAllPStarFlat[highestStableIndicesWhenFourPossible-1, fourPossible]
          thirdHighestStableEstimatesWhenFourPossible = resForAllPStarFlat[highestStableIndicesWhenFourPossible-2, fourPossible]
          # Note: using -4 corresponds to the next evenly spaced point:
          fifthHighestStableEstimatesWhenFourPossible = resForAllPStarFlat[highestStableIndicesWhenFourPossible-4, fourPossible]

          maxP=allPStar[highestStableIndicesWhenFourPossible]
          deltasWhenFourPossible = maxP / 4.0

          resultFlat[fourPossible] = quadraticEstimateOfMaxima(fifthHighestStableEstimatesWhenFourPossible**(maxP-3*deltasWhenFourPossible), thirdHighestStableEstimatesWhenFourPossible**(maxP-2*deltasWhenFourPossible), secondHighestStableEstimatesWhenFourPossible**(maxP-deltasWhenFourPossible), highestStableEstimatesWhenFourPossible**maxP, deltasWhenFourPossible)

  usedPStarIndices = set(highestStablePStarIndexFlat)
  if piecewiseScalingMode == 'lowerBound':
      # Compute the geometric mean between the upper bound (i.e.,
      # unscaled) and the lower bound (dividing by k**(1/p):
      kM = fftconvolve(np.ones_like(xPrime), np.ones_like(yPrime))
      for pStarIndex in usedPStarIndices:
          resForAllPStarFlat[pStarIndex] /= np.sqrt(kM**(1.0/allPStar[pStarIndex]))

  if piecewiseScalingMode == 'localLinear':
      # Take a 2 point linear regression using the min approx. and max
      # approx. values in each contour (compute the slope and bias by
      # computing the exact values only on those min and max indices):
      for pStarIndex in usedPStarIndices:
          contourIndices = allIndicesFlat[ highestStablePStarIndexFlat == pStarIndex ]
          approxInContourFlat = resultFlat[contourIndices]

          minIndRelToContour = argmin(approxInContourFlat)
          maxIndRelToContour = argmax(approxInContourFlat)

          approxMin = approxInContourFlat[minIndRelToContour]
          approxMax = approxInContourFlat[maxIndRelToContour]

          exactMin = maxConvolveAtIndex(xPrime, yPrime, flatIndexToShapedIndexTuple(contourIndices[minIndRelToContour], resultShape))
          exactMax = maxConvolveAtIndex(xPrime, yPrime, flatIndexToShapedIndexTuple(contourIndices[maxIndRelToContour], resultShape))

          scale = 1.0
          bias = 0.0
          if np.fabs(approxMax - approxMin) < 2*machinePrecisionTauFFT:
              # If the window is extremely narrow, don't bother computing a line:
              if approxMax > 0.0:
                  scale = exactMax / approxMax
          else:
              scale = (exactMax - exactMin) / (approxMax - approxMin)
              bias = exactMin - scale*approxMin

          # Apply the scales for this contour:
          resultFlat[contourIndices] *= scale
          resultFlat[contourIndices] += bias

  return resultFlat.reshape( resultShape )

def smartConvolve(x,y):
    if naiveConvolutionCost(x,y) > fftConvolutionCost(x,y):
        return fftconvolve(x,y)
    return convolve(x,y)

def smartMaxConvolve(x, y, approximationMode='projection', **Kwargs):
    assert(approximationMode in ('p-norm', 'projection'))

    x = np.array(x,np.float)
    y = np.array(y,np.float)

    xFlat = x.flatten()
    yFlat = y.flatten()
    n = max(len(xFlat), len(yFlat))

    # Only allow fast max convolution when vectors are nonnegative
    # (note that even very small negative values are treated as
    # negatives, and so you may want to print a warning if the slow
    # version is used):
    if min(xFlat) >= 0.0 and min(yFlat) >= 0.0 and naiveConvolutionCost(x,y) > fftMaxConvolutionCost(x,y):
        maxX = max(xFlat)
        maxY = max(yFlat)
        xPrime = x/maxX
        yPrime = y/maxY

        if approximationMode == 'p-norm':
          logBaseToCalculatePStarMax = 1 + machinePrecisionTauFFT**(1.0/4)
          # Ensures the absolute error in the max contour is smaller
          # than the sqrt absolute error from any middle contour (as
          # specified in Pfeuffer and Serang 2015):
          maxPStar = np.log2( n ) / np.log2(logBaseToCalculatePStarMax)

          return fftNonnegMaxConvolvePiecewise(xPrime, yPrime, maxPStar, normEstimationMode='bigP', **Kwargs)*maxX*maxY
        else:
          # approximationMode == 'projection'
          maxPStar = np.log2(0.7) / np.log2( 0.99 )
          return fftNonnegMaxConvolvePiecewise(xPrime, yPrime, maxPStar, normEstimationMode='extrapolate', **Kwargs)*maxX*maxY

    # When the argument vectors do not satisfy nonnegativity, simply
    # use naive max-convolution (this may be intensive-- note: it may
    # be good to print a warning here):
    return maxConvolve(x, y)


# Vectorized functions to compute low-order (linear and quadratic)
# nullspaces and zeros using closed forms (for speed):

def linearEstimateOfMaxima(a, b, deltas):
    return maxLinearRoots(a, b)**(1.0/deltas)

def maxLinearRoots(a, b):
    result = np.zeros_like(a)

    nonzeroDenominator = np.fabs(a) > machinePrecisionTauDivide
    result[ nonzeroDenominator ] = b[nonzeroDenominator]/a[nonzeroDenominator]

    zeroDenominator = np.fabs(a) <= machinePrecisionTauDivide
    # When using the p-norm estimate, delta is maxP/4, so ^(1/maxP) =
    # ^(1/(4*delta)); the ^(1/delta) will be done later, but the
    # ^(1/4) can be done now:
    result[ zeroDenominator ] = b[zeroDenominator]**(1.0/4)

    # returns b/a or b when a = 0:
    return result

def quadraticEstimateOfMaxima(a, b, c, d, deltas):
    return maxQuadraticRoots(a, b, c, d)**(1.0/deltas)

def maxQuadraticRoots(a, b, c, d):
    indices = np.arange(len(a))
    null0 = a*c-b**2
    null1 = b*c-a*d
    null2 = b*d-c**2
    # Now solve null0 x^2 + null1 x + null2 = 0:
    # Note: quadratic term null0 should be >= 0 (see paper).

    result = np.zeros(len(a))

    preRootValues = null1**2 - 4*null0*null2
    stableQuadratic = (null0 > machinePrecisionTauDivide) & (preRootValues >= 0.0)
    indicesWithStableQuadratic = indices[ stableQuadratic ]
    indicesWithUnstableQuadratic = indices[ np.logical_not( stableQuadratic ) ]

    # Solve quadratic case where denominator is nonzero and argument of square root is nonnegative:
    result[indicesWithStableQuadratic] = (-null1[indicesWithStableQuadratic] + np.sqrt(preRootValues[indicesWithStableQuadratic]))/(2*null0[indicesWithStableQuadratic])

    # Solve linear case otherwise:
    result[indicesWithUnstableQuadratic] = maxLinearRoots(c[indicesWithUnstableQuadratic], d[indicesWithUnstableQuadratic])
    return result
    
import random
from time import time
if __name__ == '__main__':
    print 'Problem 1 (small array max-convolution):'
    a1 = np.array([2,1,3,4], float)
    a2 = np.array([1,0.5,4,2], float)
    exact = maxConvolve(a1,a2)
    print 'Naive (exact) result:', exact
    approx = smartMaxConvolve(a1,a2, piecewiseScalingMode='localLinear')
    print 'Approx:', approx
    print ''

    print 'Problem 2 (large array):'
    arrayK = 1024
    a1 = np.zeros(arrayK)
    a2 = np.zeros(arrayK)
    for r in xrange(arrayK):
        a1[r] = random.uniform(0.1,1)
        a2[r] = random.uniform(0.1,1)

    t0 = time()
    exact = maxConvolve(a1,a2)
    print 'Naive (exact) result:', exact
    t1 = time()
    print 'Took', t1-t0, 'seconds'
    approx = smartMaxConvolve(a1,a2, approximationMode='p-norm', piecewiseScalingMode='localLinear')
    print 'Approx (p-norm):', approx
    t2 = time()
    print 'Took', t2-t1, 'seconds'
    approxProj = smartMaxConvolve(a1,a2, approximationMode='projection', piecewiseScalingMode='localLinear')
    print 'Approx (null space projection):', approxProj
    t3 = time()
    print 'Took', t3-t2, 'seconds'

    print ''

    print 'Problem 3 (matrix max-convolution):'
    matrixK = 128
    m1 = np.zeros( (matrixK,matrixK) )
    m2 = np.zeros( (matrixK,matrixK) )
    for r in xrange(matrixK):
        for c in xrange(matrixK):
            m1[r,c] = random.uniform(0.1,1)
            m2[r,c] = random.uniform(0.1,1)

    t0 = time()
    exact = maxConvolve(m1,m2)
    print 'Naive (exact) result:', exact
    t1 = time()
    print 'Took', t1-t0, 'seconds'
    approx = smartMaxConvolve(m1,m2, approximationMode='p-norm', piecewiseScalingMode='localLinear')
    print 'Approx (p-norm):', approx
    t2 = time()
    print 'Took', t2-t1, 'seconds'
    approxProj = smartMaxConvolve(m1,m2, approximationMode='projection', piecewiseScalingMode='localLinear')
    print 'Approx (null space projection):', approxProj
    t3 = time()
    print 'Took', t3-t2, 'seconds'
